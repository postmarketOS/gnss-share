module gitlab.com/postmarketOS/gnss-share

go 1.15

require (
	github.com/godbus/dbus/v5 v5.1.0
	github.com/google/uuid v1.3.0
	github.com/pelletier/go-toml v1.9.4
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
)

require golang.org/x/sys v0.1.0 // indirect
